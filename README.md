# Leaf conductance model for ∆₄₇ of CO₂

#### Description

The leaf conductance model calculates steady-state isotopic fractionation in δ¹³C, δ¹⁸O and ∆₄₇ for a given assimilation rate and set of resistances. A detailed model description is currently in preparation.

#### Publications

The leaf conductance model was used in the following publication:

* Adnew, G. A., Hofmann, M. E. G., Pons, T. L., Koren, G., Ziegler, M., Lourens, L. J., and Röckmann, T.: Leaf scale quantification of the effect of photosynthetic gas exchange on ∆₄₇ of CO₂, *Scientific Reports* 11, 14023, [https://doi.org/10.1038/s41598-021-93092-0](https://doi.org/10.1038/s41598-021-93092-0), (2021).

#### Model use

The model is freely available for use. Limited support is available on request from g.b.koren@uu.nl. The model can be cited as

> Koren, G., Adnew, G. A., Röckmann, T., and Peters, W.: Leaf conductance model for ∆₄₇ of CO₂, [https://git.wur.nl/leaf_model](https://git.wur.nl/leaf_model), (2021)
